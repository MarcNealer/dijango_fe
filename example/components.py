from djangofe.base_components import *
from djangofe.django_components import *


class Div(FeComponentBase):

    def beginning(self):
        line = list()
        line.append('<div')
        for items in self.attributes:
            line.append(" %s='%s'" % (items, self.template['attributes'][items]))
        line.append('>')
        if 'content' in self.params:
            line.append(self.params['content'])
        return ''.join(line)

    def end(self):
        if 'end-content' in self.params:
            return "%s</div>" % self.params['end-content']
        else:
            return '</div>'
