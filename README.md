Python Frontend Compiler
========================

Introduction
++++++++++++

I'm a python programmer but I have to deal with Front end elements all the time.
However I got fed up with the need to go through file after file and create file after file with the same
elements in them. A basic concept of python is DRY, Don't Repeat Yourself. However with HTML and JS its a constant.
I just want a wy I can deal with HTML and JS in a Pyronic way. Thus the Python Front End compiler.

How it works
++++++++++++

I looked at html as a series of chunks or components, but each component had nested components in it as well.
Thus for this system to work, I needed Python code to create each component and a system to say what component is to be
run when.

Thus I use what we call the JSON build files to say what components to run in what order, and to pass parameters etc to
each component.

The Components are simple python classes based on the Abstract class FeComponentBase. These classes not only build the text
to be added, but also start any child components as well.