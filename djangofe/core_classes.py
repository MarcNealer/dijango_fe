from abc import ABCMeta, abstractmethod
import importlib


class FeComplier:
    def __init__(self, json_template, build_name, config):
        self.template = json_template
        self.config = config
        self.config['build_name'] = build_name
        component_module = "%s.components" % config['build_path'].replace('/', '.')
        self.module = importlib.import_module(component_module)
        self.snippet = []

    def compile(self):
        for items in self.template:
            self.validate(items)
            component = getattr(self.module, items['component'])
            self.snippet.append((component(items, self.config)).compile())
        return "".join(self.snippet)

    @staticmethod
    def validate(item):
        if 'component' in item.keys():
            for items in item.keys():
                if items not in ['component', 'params', 'attributes', 'children']:
                    raise ValueError('Invalid key in component: component=%s, key=%s' % (item['component'], items))
            return True
        else:
            print("component not specified")
            raise ValueError("Invalid component : %s" % item)


class FeComponentBase(metaclass=ABCMeta):
    """
    This is an abstract base method for Fe components. Use this method to create the FeComponents
    Beginning and end methods must be overridden.

    validate_params and validate_attributes are optional methods to override to check the params and attributes
    validate the component params
    """
    def __init__(self, json_snippet, config):
        self.template = json_snippet
        self.config = config
        self.child = None
        self.params =[]
        self.attributes = []
        self.validate_params()
        self.validate_attributes()
        self.compile()

    def compile(self):
        if "children" in self.template:
            self.child = FeComplier(self.template['children'],self.config['build_name'], self.config).compile()
        if self.child:
            return "%s\n%s\n%s" % (self.beginning(), self.middle(), self.end())
        else:
            return "%s\n%s" % (self.beginning(), self.end())

    def validate_params(self):
        if 'params' in self.template:
            self.params = self.template['params']
        return True

    def validate_attributes(self):
        if 'attributes' in self.template:
            self.attributes = self.template['attributes']
        return True

    @abstractmethod
    def beginning(self):
        pass

    @abstractmethod
    def end(self):
        pass

    def middle(self):
        if self.child:
            self.child.replace("\n", "\t\n")
            return self.child
        else:
            return ""

    def __params_req__(self, component, params):
        for param in params:
            if param not in self.params:
                raise ValueError('%s parameter is required for %s component' % (param, component))

    def __attrib_req__(self, component, params):
        for param in params:
            if param not in self.params:
                raise ValueError('%s attribute is required for %s component' % (param, component))
