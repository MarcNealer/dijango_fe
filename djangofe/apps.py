from django.apps import AppConfig


class DjangofeConfig(AppConfig):
    name = 'djangofe'
