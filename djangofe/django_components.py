from djangofe.core_classes import FeComponentBase
from djangofe.core_classes import FeComplier
import json
from os.path import join
from django.template.loader import render_to_string

class FeDjangoTemplate(FeComponentBase):

    def beginning(self):
        context = self.config
        context.update(self.params)
        context['children'] = self.child
        return render_to_string(join(self.config['build_name'],'templates', self.params['template']), context=context)

    def end(self):
        pass
