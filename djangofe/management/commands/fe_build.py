from django.db.models import Model
from django.core.management.base import BaseCommand, CommandError
from djangofe.core_classes import FeComplier
import sys
import json
from os import listdir, makedirs
from os.path import isfile, join, dirname


class Command(BaseCommand):
    help = 'create a SuperUser called init_user with email init@bcmfort.com'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('build_path')

    def handle(self, *args, **options):
        self.build_path =options['build_path']
        self.projects = self.__read_projects__()
        self.__run_projects__(self.projects)

    def __run_projects__(self, projects):
        for project in projects:
            print('Build %s Started' % project[1])
            config = self.__read_config__(project[0])
            self.__run_builds__(project, config)
            print('Build %s Completed' % project[1])

    def __run_builds__(self, project, config):
        builds = [f for f in listdir("%s/builds" % project[0]) if '.json' in f]
        for build in builds:
            template = json.load(open(join(project[0],"builds",build), "r"))
            results = FeComplier(template, project[1], config).compile()
            filename = "%s%s" % (config['output'], build.replace('.json', config['file_type']))
            makedirs(dirname(filename), exist_ok=True)
            f = open(filename, "w")
            f.write(results)
            f.close()

    def __read_projects__(self):
        return [(join(self.build_path,f), f) for f in listdir(self.build_path) if not isfile(join(self.build_path, f))]

    def __read_config__(self, build):
        config = json.load(open("%s/config.json" % build, "r"))
        if 'output' not in config:
            raise ValueError("No output parameter in config for %s" % build)

        if "file_type" not in config:
            raise ValueError("No file_type parameter in config for %s" % build)
        config['file_path'] = build
        config['build_path'] = self.build_path
        return config