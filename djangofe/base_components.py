from djangofe.core_classes import FeComponentBase
from djangofe.core_classes import FeComplier
import json
from os.path import join


class FeInclude(FeComponentBase):

    def compile(self):
        template = json.load(open(join(self.config['file_path','includes', self.params['file']]), 'r'))
        self.child = FeComplier(template, self.config).compile()
        return "%s\n" % self.middle()

    def beginning(self):
        pass

    def end(self):
        pass
